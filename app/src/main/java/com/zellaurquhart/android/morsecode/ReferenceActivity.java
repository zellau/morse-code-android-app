package com.zellaurquhart.android.morsecode;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.Timer;

/**
 * Created by Zella on 12/14/15.
 */
public class ReferenceActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        MenuBar menuBar = new MenuBar(this, "Reference");
        menuBar.setBackListener(new MenuBar.BackListener() {
            @Override
            public void backListener() {
                finish();
            }
        });

        RelativeLayout textLayout = new RelativeLayout(this);
        textLayout.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView letters = new TextView(this);
        letters.setTextColor(Color.BLACK);
        letters.setTextSize(20);
        letters.setText(R.string.reference_chars);
        letters.setGravity(Gravity.RIGHT);

        TextView morse = new TextView(this);
        if (Build.VERSION.SDK_INT > 20) {
            morse.setLetterSpacing(.08f);
        }
        morse.setTextColor(Color.BLACK);
        morse.setTextSize(20);
        morse.setText(R.string.reference_morse);
        morse.setGravity(Gravity.LEFT);

        RelativeLayout.LayoutParams lettersLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lettersLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        RelativeLayout.LayoutParams morseLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        morseLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        //ViewGroup.LayoutParams morseLayoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);

        textLayout.addView(letters, lettersLayoutParams);
        textLayout.addView(morse, morseLayoutParams);

        ScrollView textScroll = new ScrollView(this);
        textScroll.addView(textLayout);

        layout.addView(menuBar);
        layout.addView(textScroll);

        setContentView(layout);
    }
}
