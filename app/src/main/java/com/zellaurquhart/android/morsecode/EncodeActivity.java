package com.zellaurquhart.android.morsecode;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class EncodeActivity extends AppCompatActivity {
    EditText messageView;
    ImageButton playButton;
    ImageButton lightButton;
    ImageButton soundButton;
    ImageButton vibrateButton;
    int onColor = Color.LTGRAY;
    int offColor = Color.TRANSPARENT;

    MorseCodeSound playMorse;

    SharedPreferences preferences;
    boolean soundOn;
    boolean vibrateOn;
    boolean lightOn;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        playMorse = new MorseCodeSound(this, AudioManager.STREAM_MUSIC);

        preferences = getSharedPreferences("settings", MODE_PRIVATE);
        soundOn = preferences.getBoolean("SoundOn", true);
        lightOn = preferences.getBoolean("LightOn", false);
        vibrateOn = preferences.getBoolean("VibrateOn", false);
        playMorse.setSoundOn(soundOn);
        playMorse.setLightOn(lightOn);
        playMorse.setVibrateOn(vibrateOn);

        LinearLayout layout = new LinearLayout(this);
        layout.setGravity(Gravity.TOP);
        layout.setOrientation(LinearLayout.VERTICAL);

        MenuBar menuBar = new MenuBar(this, "Encode");
        layout.addView(menuBar);
        menuBar.setBackListener(new MenuBar.BackListener() {
            @Override
            public void backListener() {
                finish();
            }
        });

        messageView = new EditText(this);
        messageView.setId(100);
        messageView.setGravity(Gravity.TOP);
        LinearLayout.LayoutParams messageParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        messageParams.weight = 2;
        messageView.setTextSize(25);
        messageView.setTextColor(Color.BLACK);
        layout.addView(messageView, messageParams);

        RelativeLayout buttonsLayout = new RelativeLayout(this);
        buttonsLayout.setGravity(Gravity.TOP);
        LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        buttonLayoutParams.weight = 5;

        soundButton = new ImageButton(this);
        soundButton.setBackgroundColor(offColor);
        if (soundOn){
            soundButton.setBackgroundColor(onColor);
        }
        soundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playMorse.getSound()) {
                    playMorse.setSoundOn(false);
                    soundButton.setBackgroundColor(offColor);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("SoundOn", false);
                    editor.commit();
                } else {
                    playMorse.setSoundOn(true);
                    soundButton.setBackgroundColor(onColor);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("SoundOn", true);
                    editor.commit();
                }
            }
        });
        soundButton.setId(20);
        soundButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
        soundButton.setImageResource(R.drawable.audio);
        RelativeLayout.LayoutParams soundLayoutParams = new RelativeLayout.LayoutParams((int) (50 * getResources().getDisplayMetrics().density), (int) (50 * getResources().getDisplayMetrics().density));
        //soundLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        soundLayoutParams.setMargins(0, 0, 0, 0);

        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            lightButton = new ImageButton(this);
            lightButton.setBackgroundColor(offColor);
            if (lightOn){
                lightButton.setBackgroundColor(onColor);
            }
            lightButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (playMorse.getLight()) {
                        playMorse.setLightOn(false);
                        lightButton.setBackgroundColor(offColor);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("LightOn", false);
                        editor.commit();
                    } else {
                        playMorse.setLightOn(true);
                        lightButton.setBackgroundColor(onColor);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("LightOn", true);
                        editor.commit();
                    }
                }
            });
            lightButton.setId(10);
            lightButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
            lightButton.setImageResource(R.drawable.light);
            RelativeLayout.LayoutParams lightLayoutParams = new RelativeLayout.LayoutParams((int) (50 * getResources().getDisplayMetrics().density), (int) (50 * getResources().getDisplayMetrics().density));
            lightLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            lightLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            lightLayoutParams.setMargins(0, 0, 0, 0);
            buttonsLayout.addView(lightButton, lightLayoutParams);

            soundLayoutParams.addRule(RelativeLayout.RIGHT_OF, lightButton.getId());
        }

        buttonsLayout.addView(soundButton, soundLayoutParams);

        vibrateButton = new ImageButton(this);
        vibrateButton.setBackgroundColor(offColor);
        if (vibrateOn){
            vibrateButton.setBackgroundColor(onColor);
        }
        vibrateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playMorse.getVibrate()) {
                    playMorse.setVibrateOn(false);
                    vibrateButton.setBackgroundColor(offColor);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("VibrateOn", false);
                    editor.commit();
                } else {
                    playMorse.setVibrateOn(true);
                    vibrateButton.setBackgroundColor(onColor);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("VibrateOn", true);
                    editor.commit();
                }
            }
        });
        vibrateButton.setScaleType(ImageView.ScaleType.FIT_CENTER);
        vibrateButton.setImageResource(R.drawable.lightning);
        RelativeLayout.LayoutParams vibrateLayoutParams = new RelativeLayout.LayoutParams((int) (50 * getResources().getDisplayMetrics().density), (int) (50 * getResources().getDisplayMetrics().density));
        vibrateLayoutParams.setMargins(0, 0, 0, 0);
        vibrateLayoutParams.addRule(RelativeLayout.RIGHT_OF, soundButton.getId());
        buttonsLayout.addView(vibrateButton, vibrateLayoutParams);


        playButton = new ImageButton(this);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = messageView.getText().toString().toLowerCase();
                playMorse.startNewSound(string);
            }
        });
        playButton.setScaleType(ImageView.ScaleType.FIT_END);
        playButton.setImageResource(R.drawable.play_button);
        playButton.setBackgroundColor(Color.TRANSPARENT);
        RelativeLayout.LayoutParams playLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        playLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        playLayoutParams.setMargins(0, 0, 0, 0);
        buttonsLayout.addView(playButton, playLayoutParams);

        layout.addView(buttonsLayout, buttonLayoutParams);

        setContentView(layout);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        playMorse.destroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        outState.putString("message", messageView.getText().toString());
        outState.putBoolean("light", playMorse.getLight());
        outState.putBoolean("sound", playMorse.getSound());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle inState){
        super.onRestoreInstanceState(inState);

        playMorse.setLightOn(inState.getBoolean("light"));
        if (playMorse.getLight()) {
            lightButton.setBackgroundColor(onColor);
        } else {
            lightButton.setBackgroundColor(offColor);
        }

        playMorse.setSoundOn(inState.getBoolean("sound"));
        if (playMorse.getSound()) {
            soundButton.setBackgroundColor(onColor);
        } else {
            soundButton.setBackgroundColor(offColor);
        }
    }

}
