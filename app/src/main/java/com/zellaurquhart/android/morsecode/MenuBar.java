package com.zellaurquhart.android.morsecode;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ActionMenuView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Zella on 12/8/15.
 */
public class MenuBar extends RelativeLayout {
    Button back;
    TextView activityName;
    BackListener backListener;

    public MenuBar(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        init(context);
    }

    public MenuBar(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);

        init(context);
    }

    public MenuBar(Context context) {
        super(context);

        init(context);
    }

    public MenuBar(Context context, String name) {
        super(context);

        init(context);
        setName(name);
    }

    private void init(Context context){
        final Context c = context;

        //setGravity(Gravity.CENTER_VERTICAL);
        setBackgroundColor(0xFF222222);
        ViewGroup.LayoutParams menuParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setLayoutParams(menuParams);

        final Intent homeIntent = new Intent(context, HomeActivity.class);
        //setGravity(Gravity.LEFT);
        back = new Button(context);
        back.setGravity(Gravity.CENTER_VERTICAL);
        back.setBackgroundColor(Color.TRANSPARENT);
        back.setTextColor(Color.WHITE);
        back.setTextSize(30);
        back.setPadding(20, 0, 0, 0);
        back.setText("<");

        back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getBackListener().backListener();
            }
        });
        RelativeLayout.LayoutParams buttonParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        buttonParams.addRule(CENTER_VERTICAL, TRUE);
        addView(back, buttonParams);

        activityName = new TextView(context);
        activityName.setGravity(Gravity.CENTER_VERTICAL);
//        activityName.setText(name);
        activityName.setTextColor(Color.WHITE);
        activityName.setTextSize(25);
        activityName.setPadding(0, 15, 0, 15);
        RelativeLayout.LayoutParams titleParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        titleParams.addRule(CENTER_IN_PARENT,TRUE);
        addView(activityName, titleParams);
    }

    public void setName(String name){
        activityName.setText(name);
    }

    public void setBackListener(BackListener listener){
        backListener = listener;
    }
    public BackListener getBackListener(){
        return backListener;
    }

    public interface BackListener{
        public void backListener();
    }
}
