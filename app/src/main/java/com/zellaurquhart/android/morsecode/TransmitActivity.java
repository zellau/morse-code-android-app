package com.zellaurquhart.android.morsecode;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import android.provider.ContactsContract.Contacts;

public class TransmitActivity extends AppCompatActivity {
    SoundPool morseNote = new SoundPool(2, AudioManager.STREAM_MUSIC, 0);
    int soundId;
    int streamId;
    long soundTimer;
    long gapTimer;
    Timer gapWait;
    String character = "";
    String message = "";
    String dot = ".";
    String dash = "-";
    TextView transmittedMessage;
    boolean decoded;

    Cursor cursor;
    String phone;

    private static final int CONTACT_PICKER_RESULT = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        soundId = morseNote.load(this, R.raw.morse_note, 1);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transmit);

        MenuBar menuBar = (MenuBar)findViewById(R.id.menu_bar);
        menuBar.setName("Transmit");
        menuBar.setBackListener(new MenuBar.BackListener() {
            @Override
            public void backListener() {
                finish();
            }
        });

        transmittedMessage = (TextView)findViewById(R.id.transmitted_message);//new TextView(this);
        transmittedMessage.setTextSize(25);
        transmittedMessage.setText(message);
        transmittedMessage.setTextColor(Color.BLACK);

        Button backspace = (Button)findViewById(R.id.backspace);
        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (message.length() > 0) {
                    message = message.substring(0, message.length() - 1);
                    transmittedMessage.setText(message);
                    if (message.contains("-")) {
                        transmittedMessage.setTextColor(Color.RED);
                    } else {
                        transmittedMessage.setTextColor(Color.BLACK);
                    }
                }
            }
        });

        final ImageButton telegraphButton = (ImageButton)findViewById(R.id.telegraph_button);
        telegraphButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    telegraphButton.setImageResource(R.drawable.telegraphkeyred);
                    streamId = morseNote.play(soundId, .5f, .5f, 1, 0, 1);
                    soundTimer = Calendar.getInstance().getTimeInMillis();
                    if (!decoded && gapTimer > 0) {
                        gapTimer = Calendar.getInstance().getTimeInMillis() - gapTimer;
                        final char letter;
                        if (gapTimer > 300) { //new letter
                            letter = decodeLetter();
                            if (letter == '-') {
                                transmittedMessage.setTextColor(Color.RED);
                            }
                            message += letter;
                            character = "";
                        }
                        if (gapTimer > 2500) { //new word (apparently should be 700, but that seems much to fast)
                                                //i am teaching my students bad skills
                            character = " ";
                        }
                        transmittedMessage.setText(message);
                        decoded = true;
                    }
                }
                if (event.getAction() == MotionEvent.ACTION_UP){
                    telegraphButton.setImageResource(R.drawable.telegraphkeyblack);
                    morseNote.pause(streamId);
                    soundTimer = Calendar.getInstance().getTimeInMillis() - soundTimer;
                    if (soundTimer > 300){
                        character += dash;
                    } else {
                        character += dot;
                    }
                    //transmittedMessage.setText(character);
                    gapWait = new Timer();
                    gapWait.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    decodeWord();
                                }
                            });
                        }
                    }, 3000);
                    decoded = false;
                    gapTimer = Calendar.getInstance().getTimeInMillis();
                }
                return false;
            }
        });

        Button sendButton = (Button)findViewById(R.id.send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CONNECTION_SERVICE))
                //getWindow().setFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM, WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                //phoneNumber.show();
                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, Contacts.CONTENT_URI);
                startActivityForResult(contactPickerIntent, CONTACT_PICKER_RESULT);
            }
        });
    }

    private void decodeWord(){
        if (!decoded) {
            char letter = decodeLetter();
            if (letter == '-') {
                transmittedMessage.setTextColor(Color.RED);
            }
            message += letter;
            message += ' ';
            transmittedMessage.setText(message);
            decoded = true;
            character = "";
        }
    }

    private char decodeLetter(){
        switch (character){
            case ".-":
                return 'a';
            case "-...":
                return 'b';
            case "-.-.":
                return 'c';
            case "-..":
                return 'd';
            case ".":
                return 'e';
            case "..-.":
                return 'f';
            case "--.":
                return 'g';
            case "....":
                return 'h';
            case "..":
                return 'i';
            case ".---":
                return 'j';
            case "-.-":
                return 'k';
            case ".-..":
                return 'l';
            case "--":
                return 'm';
            case "-.":
                return 'n';
            case "---":
                return 'o';
            case ".--.":
                return 'p';
            case "--.-":
                return 'q';
            case ".-.":
                return 'r';
            case "...":
                return 's';
            case "-":
                return 't';
            case "..-":
                return 'u';
            case "...-":
                return 'v';
            case ".--":
                return 'w';
            case "-..-":
                return 'x';
            case "-.--":
                return 'y';
            case "--..":
                return 'z';
            case "----":
                return '0';
            case ".----":
                return '1';
            case "..---":
                return '2';
            case "...--":
                return '3';
            case "....-":
                return '4';
            case ".....":
                return '5';
            case "-....":
                return '6';
            case "--...":
                return '7';
            case "---..":
                return '8';
            case "----.":
                return '9';
            case ".-.-.-":
                return '.';
            case "--..--":
                return ',';
            case "..--..":
                return '?';
            case "-.-.--":
                return '!';
        }
        return '-';
    }

    //copied most of this code from http://stackoverflow.com/questions/8578689/sending-text-messages-programmatically-in-android
    private void sendSMS(String phoneNumber, String message)
    {
        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                new Intent("SMS_SENT"), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent("SMS_DELIVERED"), 0);

//        registerReceiver(new BroadcastReceiver(){
//            @Override
//            public void onReceive(Context arg0, Intent arg1) {
//                switch (getResultCode())
//                {
//                    case Activity.RESULT_OK:
//                        Toast.makeText(getBaseContext(), "SMS sent",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                        Toast.makeText(getBaseContext(), "Generic failure",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NO_SERVICE:
//                        Toast.makeText(getBaseContext(), "No service",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_NULL_PDU:
//                        Toast.makeText(getBaseContext(), "Null PDU",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case SmsManager.RESULT_ERROR_RADIO_OFF:
//                        Toast.makeText(getBaseContext(), "Radio off",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        }, new IntentFilter("SMS_SENT"));
//
//        //---when the SMS has been delivered---
//        registerReceiver(new BroadcastReceiver(){
//            @Override
//            public void onReceive(Context arg0, Intent arg1) {
//                switch (getResultCode())
//                {
//                    case Activity.RESULT_OK:
//                        Toast.makeText(getBaseContext(), "SMS delivered",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                    case Activity.RESULT_CANCELED:
//                        Toast.makeText(getBaseContext(), "SMS not delivered",
//                                Toast.LENGTH_SHORT).show();
//                        break;
//                }
//            }
//        }, new IntentFilter("SMS_DELIVERED"));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        outState.putString("message", message);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle inState){
        super.onRestoreInstanceState(inState);
        message = inState.getString("message");
        transmittedMessage.setText(message);
        if (message.contains("-")){
            transmittedMessage.setTextColor(Color.RED);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CONTACT_PICKER_RESULT:
//                    Bundle extras = data.getExtras();
//                    Set keys = extras.keySet();
//                    Iterator iterate = keys.iterator();
//                    while (iterate.hasNext()) {
//                        String key = iterate.next().toString();
//                        Log.v("ContactPicker", key + "[" + extras.get(key) + "]");
//                    }
                    Uri result = data.getData();
                    Log.v("ContactPicker", "Got a result: " + result.toString());
                    //content://com.android.contacts/contacts/lookup/1602ic83f9908f4bc159/598
                    String id = result.getLastPathSegment();
                    cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID+"=?", new String[]{id}, null);
                    //cursor = getContentResolver().query(Telephony.Sms.CONTENT_URI, null, Telephony.Sms._ID, new String[]{id}, null);//(Email.CONTENT_URI, null, Email.CONTACT_ID + "=?", new String[]{id}, null);
                    if (cursor.moveToFirst()) {
                        int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA);
                        phone = cursor.getString(numberIndex);
                        Log.v("ContactPicker", "Got number: " + phone);
                        sendSMS(phone, message);
                    }
                    break;
            }

        } else {
            // gracefully handle failure
            Log.w("ContactPicker", "Warning: activity result not ok");
        }
    }

}
