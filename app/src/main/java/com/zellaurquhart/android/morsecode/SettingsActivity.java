package com.zellaurquhart.android.morsecode;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    String _difficulty = "Difficulty";
    String _speed = "Speed";
    String _tone = "Tone";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        preferences = getSharedPreferences("settings", MODE_PRIVATE);
        editor = preferences.edit();
        final float speed = preferences.getFloat(_speed, 1);
        int difficulty = preferences.getInt(_difficulty, 0);
        int tone = preferences.getInt(_tone, 0);

        MenuBar menuBar = (MenuBar)findViewById(R.id.menu_bar);
        menuBar.setName("Settings");
        menuBar.setBackListener(new MenuBar.BackListener() {
            @Override
            public void backListener() {
                finish();
            }
        });

        final Button slowSpeedButton = (Button)findViewById(R.id.slow_speed);
        final Button fastSpeedButton = (Button)findViewById(R.id.fast_speed);

        slowSpeedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (speed>.5f) {
                    editor.putFloat(_speed, speed - .5f);
                } else {
                    editor.putFloat(_speed, .5f);

                }
                editor.commit();

            }
        });

        fastSpeedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putFloat(_speed, speed + .5f);
                editor.commit();
            }
        });

        final Button easyButton = (Button)findViewById(R.id.easy);
        final Button medButton = (Button)findViewById(R.id.med_hard);
        final Button hardButton = (Button)findViewById(R.id.hard);

        if (difficulty == 1){
            medButton.setBackgroundColor(Color.DKGRAY);
        } else if (difficulty == 2){
            hardButton.setBackgroundColor(Color.DKGRAY);
        } else {
            easyButton.setBackgroundColor(Color.DKGRAY);
        }

        easyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt(_difficulty, 0);
                editor.commit();
                easyButton.setBackgroundColor(Color.DKGRAY);
                medButton.setBackgroundColor(0xFFA9A9A9);
                hardButton.setBackgroundColor(0xFFA9A9A9);
            }
        });

        medButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt(_difficulty, 1);
                editor.commit();
                medButton.setBackgroundColor(Color.DKGRAY);
                easyButton.setBackgroundColor(0xFFA9A9A9);
                hardButton.setBackgroundColor(0xFFA9A9A9);
            }
        });

        hardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt(_difficulty, 2);
                editor.commit();
                hardButton.setBackgroundColor(Color.DKGRAY);
                medButton.setBackgroundColor(0xFFA9A9A9);
                easyButton.setBackgroundColor(0xFFA9A9A9);
            }
        });

        final Button noToneButton = (Button)findViewById(R.id.no_ringtone);
        final Button nameToneButton = (Button)findViewById(R.id.name_ringtone);
        final Button textToneButton = (Button)findViewById(R.id.text_ringtone);

        if (tone == 1){
            nameToneButton.setBackgroundColor(Color.DKGRAY);
        } else if (tone == 2){
            textToneButton.setBackgroundColor(Color.DKGRAY);
        } else {
            noToneButton.setBackgroundColor(Color.DKGRAY);
        }

        noToneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt(_tone, 0);
                editor.commit();
                noToneButton.setBackgroundColor(Color.DKGRAY);
                nameToneButton.setBackgroundColor(0xFFA9A9A9);
                textToneButton.setBackgroundColor(0xFFA9A9A9);
            }
        });

        nameToneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt(_tone, 1);
                editor.commit();
                nameToneButton.setBackgroundColor(Color.DKGRAY);
                noToneButton.setBackgroundColor(0xFFA9A9A9);
                textToneButton.setBackgroundColor(0xFFA9A9A9);
            }
        });

        textToneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt(_tone, 2);
                editor.commit();
                textToneButton.setBackgroundColor(Color.DKGRAY);
                nameToneButton.setBackgroundColor(0xFFA9A9A9);
                noToneButton.setBackgroundColor(0xFFA9A9A9);
            }
        });

        Button scrambleButton = (Button)findViewById(R.id.scramble_learning);
        scrambleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int scrambled = preferences.getInt("Scrambled", 0);
                if (scrambled == 0){
                    editor.putInt("Scrambled", 1);
                    editor.commit();
                } else {
                    editor.putInt("Scrambled", 0);
                    editor.commit();
                }
            }
        });

    }

}
