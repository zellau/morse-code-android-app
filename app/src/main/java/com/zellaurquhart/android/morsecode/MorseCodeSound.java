package com.zellaurquhart.android.morsecode;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Vibrator;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Zella on 12/31/15.
 */
public class MorseCodeSound {
    private SharedPreferences preferences;
    int soundId;
    SoundPool soundPool = new SoundPool(2, AudioManager.STREAM_MUSIC, 0);
    boolean light = false;
    boolean sound = true;
    boolean vibrate = false;
    boolean blocked_sound = true; //true at the start and end of the object's life; should avoid playing at bad times
    boolean blocked_light = true; //true at the start and end of the object's life; should avoid playing at bad times
    int currentAudioId = 0;     //a unique id is given to each command to play a string; this stores the audio id that should be currently playing
    Context cntxt;
    int streamType;         //is the tone being played as music or a ringtone?

    //times required for characters to play
    int shortShortTime = 500;
    int tTime = 600;
    int shortTime = 800;
    int shortishTime = 900;
    int medShortTime = 1000;
    int medTime = 1200;
    int medLongTime = 1500;
    int longishTime = 1600;
    int longTime = 1700;
    int longLongTime = 2000;
    int veryLongTime = 2200;
    int lengthBecomingRidiculousTime = 2500;

    //preference variables
    float codeSpeed;
    //camera variables
    Camera cam;
    Camera.Parameters cameraParams;

    /*
     * Create an instance of the MorseCodeSound class
     * load all the audio files and get the speed from preferences
     *
     */
    public MorseCodeSound(Context context, int music_or_ringtone){
        new loadAudioFiles().execute(context);
        cntxt = context;
        streamType = music_or_ringtone;

        preferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        codeSpeed = preferences.getFloat("Speed", 1);

        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            try {
                cam = android.hardware.Camera.open();
                cameraParams = cam.getParameters();
                blocked_light = false;
            } catch (Exception e) {

            }
        }
    }

    /*
     * Loads the audio file for each character, then sets blocked to false
     * This part takes a long time, so we do it in the background
     */
    private class loadAudioFiles extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            Context context = (Context) params[0];
            soundId = soundPool.load(context, R.raw.morse_note, 1);
            blocked_sound = false;
            return null;
        }
    }

//    /*
//     * Changes the volume of the sounds played
//     * Should be called when the volume buttons are pressed
//     */
//    public void changeVolume(float volume){
//        vol = volume;
//    }

    public boolean getLight(){
        return light;
    }

    public boolean getSound(){
        return sound;
    }

    public boolean getVibrate(){
        return vibrate;
    }

    /*
     * This method takes a boolean and sets light to that value
     */
    public void setLightOn(boolean isOn){
        light = isOn;
    }

    /*
     * This method takes a boolean and sets sound to that value
     */
    public void setSoundOn(boolean isOn){
        sound = isOn;
    }

    /*
     * This method takes a boolean and sets vibrate to that value
     */
    public void setVibrateOn(boolean isOn){
        vibrate = isOn;
    }

    /*
     * This method takes a string and a unique id and recursively plays the sound  and light for each character in the string
     * It plays the first letter of each string and gets the length of time the letter takes
     * After waiting that amount of time, it calls itself using the same audioId and the unplayed portion of the string
     * If the audioId is not the currentAudioId, a new string has started playing and the old strings are blocked.
     */
    private void playString(String string, final int audioId) {
        if (string.length() == 0){
            blocked_sound = false;
            return;
        }
        final String input = string;
        Timer finishLetter = new Timer();
        finishLetter.schedule(new TimerTask() {
            @Override
            public void run() {
                if (input.length() > 1) {
                    if (!blocked_light && audioId == currentAudioId) {
                        playString(input.substring(1), audioId);
                    }
                    blocked_sound = false;
                }
            }
        }, playLetter(input.toLowerCase().charAt(0)));
    }

    /*
     * This method takes a character as input and returns the amount of time it takes to play that character.
     * It calls playChar to actually turn the light and sound on and off at the right times.
     */
    public int playLetter(char letter) {
        switch (letter) {
            case 'a':
                playChar(".-", 0);
                return (int)(shortTime/codeSpeed);
            case 'b':
                playChar("-...", 0);
                return (int)(medTime/codeSpeed);
            case 'c':
                playChar("-.-.", 0);
                return (int)(medLongTime/codeSpeed);
            case 'd':
                playChar("-..", 0);
                return (int)(medTime/codeSpeed);
            case 'e':
                playChar(".", 0);
                return (int)(shortShortTime/codeSpeed);
            case 'f':
                playChar("..-.", 0);
                return (int)(medTime/codeSpeed);
            case 'g':
                playChar("--.", 0);
                return (int)(medTime/codeSpeed);
            case 'h':
                playChar("....", 0);
                return (int)(medShortTime/codeSpeed);
            case 'i':
                playChar("..", 0);
                return (int)(shortTime/codeSpeed);
            case 'j':
                playChar(".---", 0);
                return (int)(medLongTime/codeSpeed);
            case 'k':
                playChar("-.-", 0);
                return (int)(medTime/codeSpeed);
            case 'l':
                playChar(".-..", 0);
                return (int)(medTime/codeSpeed);
            case 'm':
                playChar("--", 0);
                return (int)(medShortTime/codeSpeed);
            case 'n':
                playChar("-.", 0);
                return (int)(shortishTime/codeSpeed);
            case 'o':
                playChar("---", 0);
                return (int)(medLongTime/codeSpeed);
            case 'p':
                playChar(".--.", 0);
                return (int)(medLongTime/codeSpeed);
            case 'q':
                playChar("--.-", 0);
                return (int)(longishTime/codeSpeed);
            case 'r':
                playChar(".-.", 0);
                return (int)(medShortTime/codeSpeed);
            case 's':
                playChar("...", 0);
                return (int)(shortishTime/codeSpeed);
            case 't':
                playChar("-", 0);
                return (int)(tTime/codeSpeed);
            case 'u':
                playChar("..-", 0);
                return (int)(medShortTime/codeSpeed);
            case 'v':
                playChar("...-", 0);
                return (int)(medTime/codeSpeed);
            case 'w':
                playChar(".--", 0);
                return (int)(medTime/codeSpeed);
            case 'x':
                playChar("-..-", 0);
                return (int)(medLongTime/codeSpeed);
            case 'y':
                playChar("-.--", 0);
                return (int)(medLongTime/codeSpeed);
            case 'z':
                playChar("--..", 0);
                return (int)(medLongTime/codeSpeed);
            case '1':
                playChar(".----", 0);
                return (int)(lengthBecomingRidiculousTime/codeSpeed);
            case '2':
                playChar("..---", 0);
                return (int)(longLongTime/codeSpeed);
            case '3':
                playChar("...--", 0);
                return (int)(longishTime/codeSpeed);
            case '4':
                playChar("....-", 0);
                return (int)(longishTime/codeSpeed);
            case '5':
                playChar(".....", 0);
                return (int)(longishTime/codeSpeed);
            case '6':
                playChar("-....", 0);
                return (int)(longishTime/codeSpeed);
            case '7':
                playChar("--...", 0);
                return (int)(longishTime/codeSpeed);
            case '8':
                playChar("---..", 0);
                return (int)(longLongTime/codeSpeed);
            case '9':
                playChar("----.", 0);
                return (int)(lengthBecomingRidiculousTime/codeSpeed);
            case '0':
                playChar("-----", 0);
                return (int)(lengthBecomingRidiculousTime/codeSpeed);
            case '.':
                playChar(".-.-.-", 0);
                return (int)(longLongTime/codeSpeed);
            case ',':
                playChar("--..--", 0);
                return (int)(veryLongTime/codeSpeed);
            case '!':
                playChar("-.-.--", 0);
                return (int)(lengthBecomingRidiculousTime/codeSpeed);
            case '?':
                playChar("..--..", 0);
                return (int)(longTime/codeSpeed);
            case ' ':
                return (int)(shortishTime/codeSpeed);
            default:
                return 0;
        }
    }

    /*
     * This method takes a sequence of dots and dashes and a time.
     * After waiting the correct amount of time, it plays the dots and dashes as light and sound.
     * It returns the total time (waitTime plus the time to play the dot or dash)
     */
    private int playChar(final String string, final int waitTime) {
        //how long will it take to play the character?
        int time;
        final boolean dot;
        if (string.charAt(0) == '.') {
            time = (int) (200 / codeSpeed);
            dot = true;
        } else if (string.charAt(0) == '-') {
            time = (int) (400 / codeSpeed);
            dot = false;
        } else { //if an incorrect character is passed in, do nothing and return 0
            return 0;
        }

        //add that time to waitTime
        //if there are more dots and dashes, call playChar again using the new time as waitTime
        int x;
        if (string.length() == 1) {
            x = time + waitTime;
        } else {
            x = playChar(string.substring(1), time + waitTime);
        }

        //after waitTime has elapsed, play the dot or dash
        Timer finishChar = new Timer();
        finishChar.schedule(new TimerTask() {
            @Override
            public void run() {
                if (dot) {
                    if (light) {
                        dotLight();
                    }
                    if (sound){
                        dotSound();
                    }
                    if (vibrate){
                        dotVibrate();
                    }
                } else {
                    if (light) {
                        dashLight();
                    }
                    if (sound){
                        dashSound();
                    }
                    if (vibrate){
                        dashVibrate();
                    }
                }
            }
        }, waitTime);
        return x;
    }

    /*
     * This method plays a dot using the camera light.
     */
    public int dotLight(){
        if (!blocked_light) {
            cameraParams.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            cam.setParameters(cameraParams);
        }
        (new Timer()).schedule(new TimerTask() {
            @Override
            public void run() {
                if (!blocked_light) {
                    cameraParams.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    cam.setParameters(cameraParams);
                }
            }
        }, (int) (100 / codeSpeed));
        return (int)(200/codeSpeed);
    }

    /*
     * This method plays a dot using the camera light.
     */
    public int dashLight(){
        if (!blocked_light) {
            cameraParams.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            cam.setParameters(cameraParams);
        }
        (new Timer()).schedule(new TimerTask() {
            @Override
            public void run() {
                if (!blocked_light) {
                    cameraParams.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    cam.setParameters(cameraParams);
                }
            }
        }, (int) (300 / codeSpeed));
        return (int)(400/codeSpeed);
    }

    /*
     * This method plays a dot using the soundpool object.
     */
    public int dotSound(){
        float vol = ((AudioManager) cntxt.getSystemService(Context.AUDIO_SERVICE)).getStreamVolume(streamType);
        if (!blocked_sound) {
            soundPool.play(soundId, vol, vol, 1, 0, 1);
        }
        (new Timer()).schedule(new TimerTask() {
            @Override
            public void run() {
                if (!blocked_sound) {
                    soundPool.autoPause();
                }
            }
        }, (int) (100 / codeSpeed));
        return (int)(200/codeSpeed);
    }

    /*
     * This method plays a dot using the soundpool object.
     */
    public int dashSound(){
        float vol = ((AudioManager) cntxt.getSystemService(Context.AUDIO_SERVICE)).getStreamVolume(streamType);
        if (!blocked_sound) {
            soundPool.play(soundId, vol, vol, 1, 0, 1);
        }
        (new Timer()).schedule(new TimerTask() {
            @Override
            public void run() {
                if (!blocked_sound) {
                    soundPool.autoPause();
                }
            }
        }, (int) (300 / codeSpeed));
        return (int)(400/codeSpeed);
    }

    /*
     * This method plays a dot using vibration.
     */
    public int dotVibrate(){
        if (!blocked_sound) {
            Vibrator v = (Vibrator) cntxt.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate((long)(100 / codeSpeed));
        }
//        (new Timer()).schedule(new TimerTask() {
//            @Override
//            public void run() {
//                if (!blocked_light) {
//                    soundPool.autoPause();
//                }
//            }
//        }, (int) (100 / codeSpeed));
        return (int)(200/codeSpeed);
    }

    /*
     * This method plays a dot using the soundpool object.
     */
    public int dashVibrate(){
        if (!blocked_sound) {
            Vibrator v = (Vibrator) cntxt.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate((long) (300 / codeSpeed));
        }
//        (new Timer()).schedule(new TimerTask() {
//            @Override
//            public void run() {
//                if (!blocked_light) {
//                    soundPool.autoPause();
//                }
//            }
//        }, (int) (300 / codeSpeed));
        return (int)(400/codeSpeed);
    }

    /*
     * This method pauses the sound that is currently playing
     * It is never used and I might get rid of it
     * If I don't get rid of it, I'll have to add something in to this and to playString to pause the entire string
     */
    public void pause(){
        soundPool.autoPause();
        currentAudioId++;
    }

    /*
     * This method takes a string and starts playing the morse code for that string.
     * If blocked is true, it waits for 300ms then tries again
     * Otherwise it stops any strings that are being played and starts playString
     */
    public void startNewSound(final String message){
        if (!blocked_sound) {
            soundPool.autoPause();
            currentAudioId++;
            playString(message, currentAudioId);
        } else {
            Timer soundsLoaded = new Timer();
            soundsLoaded.schedule(new TimerTask() {
                @Override
                public void run() {
                    startNewSound(message);
                }
            }, 300);
        }
    }

    /*
     * This method releases the soundPool resource and stops the morse code from playing
     */
    public void destroy(){
        light = false;
        sound = false;
        soundPool.autoPause();
        blocked_sound = true;
        soundPool.release();

        blocked_light = true;
        cameraParams.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        cam.setParameters(cameraParams);
        cam.release();
        cam = null;
    }

}
