package com.zellaurquhart.android.morsecode;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LearningActivity extends AppCompatActivity {
    char[] characters = new char[40];
    char[] symbols = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
            'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', ',', '?', '!'};
    int selected = 0;
    MorseCodeSound playMorse;

    int onColor = Color.LTGRAY;
    int offColor = Color.TRANSPARENT;

    SharedPreferences preferences;
    boolean soundOn;
    boolean vibrateOn;
    boolean lightOn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playMorse = new MorseCodeSound(this, AudioManager.STREAM_MUSIC);
        setContentView(R.layout.activity_learning);

        preferences = getSharedPreferences("settings", MODE_PRIVATE);
        int s = preferences.getInt("Scrambled", 0);
        if (s == 1){
            int[] used = new int[40];
            for (int i = 0; i < characters.length; i++){
                //get random index
                int j = (int)Math.floor(Math.random()*40);
                //make sure the index is new
                while (used[j]==1){
                    j = (int)Math.floor(Math.random()*40);
                }
                used[j] = 1;
                //push the character from that index onto characters
                characters[i] = symbols[j];
            }
        } else {
            characters = symbols;
        }
        soundOn = preferences.getBoolean("SoundOn", true);
        lightOn = preferences.getBoolean("LightOn", false);
        vibrateOn = preferences.getBoolean("VibrateOn", false);
        playMorse.setSoundOn(soundOn);
        playMorse.setLightOn(lightOn);
        playMorse.setVibrateOn(vibrateOn);

        MenuBar menuBar = new MenuBar(this, "Learning");
        RelativeLayout layout = (RelativeLayout)findViewById(R.id.layout);
        layout.addView(menuBar);
        menuBar.setBackListener(new MenuBar.BackListener() {
            @Override
            public void backListener() {
                finish();
            }
        });

        ImageButton replayButton = (ImageButton)findViewById(R.id.replay_button);
        replayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playMorse.startNewSound("" + characters[selected]);
            }
        });

        ImageButton backButton = (ImageButton)findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected = selected - 1;
                if (selected < 0) {
                    selected = characters.length - 1;
                }
                setScreen();
                playMorse.startNewSound("" + characters[selected]);
            }
        });

        ImageButton forwardButton = (ImageButton)findViewById(R.id.forward_button);
        forwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected = ((selected + 1) % characters.length);
                setScreen();
                playMorse.startNewSound("" + characters[selected]);
            }
        });

        ImageButton lightButton = (ImageButton) findViewById(R.id.light_button);
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            if (lightOn) {
                lightButton.setBackgroundColor(onColor);
            }
            lightButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (playMorse.getLight()) {
                        playMorse.setLightOn(false);
                        v.setBackgroundColor(offColor);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("LightOn", false);
                        editor.commit();
                    } else {
                        playMorse.setLightOn(true);
                        v.setBackgroundColor(onColor);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("LightOn", true);
                        editor.commit();
                    }
                }
            });
        } else{
            layout.removeView(lightButton);
        }

        ImageButton soundButton = (ImageButton)findViewById(R.id.sound_button);
        if (soundOn){
            soundButton.setBackgroundColor(onColor);
        }
        soundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playMorse.getSound()) {
                    playMorse.setSoundOn(false);
                    v.setBackgroundColor(offColor);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("SoundOn", false);
                    editor.commit();
                } else {
                    playMorse.setSoundOn(true);
                    v.setBackgroundColor(onColor);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("SoundOn", true);
                    editor.commit();
                }
            }
        });

        ImageButton vibrateButton = (ImageButton)findViewById(R.id.vibrate_button);
        if (vibrateOn){
            vibrateButton.setBackgroundColor(onColor);
        }
        vibrateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playMorse.getVibrate()) {
                    playMorse.setVibrateOn(false);
                    v.setBackgroundColor(offColor);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("VibrateOn", false);
                    editor.commit();
                } else {
                    playMorse.setVibrateOn(true);
                    v.setBackgroundColor(onColor);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("VibrateOn", true);
                    editor.commit();
                }
            }
        });

        TextView symbol = (TextView)findViewById(R.id.symbol);
        symbol.setText("" + characters[selected]);

        setScreen();
    }

    private void setScreen(){
        TextView symbol = (TextView) findViewById(R.id.symbol);
        symbol.setText("" + characters[selected]);

        //playMorse.startNewSound("" + characters[selected]);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        outState.putInt("selected", selected);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle inState){
        super.onRestoreInstanceState(inState);
        selected = inState.getInt("selected");
        setScreen();
    }

    @Override
    protected void onResume(){
        super.onResume();
        playMorse.startNewSound(""+characters[selected]);
    }

}
