package com.zellaurquhart.android.morsecode;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        final Intent learningIntent = new Intent(this, LearningActivity.class);
        final Intent practiceIntent = new Intent(this, PracticeActivity.class);
        final Intent transmitIntent = new Intent(this, TransmitActivity.class);
        final Intent encodeIntent = new Intent(this, EncodeActivity.class);
        final Intent settingsIntent = new Intent(this, SettingsActivity.class);
        final Intent aboutIntent = new Intent(this, AboutActivity.class);
        final Intent referenceIntent = new Intent(this, ReferenceActivity.class);

        Button learningButton = (Button)findViewById(R.id.learning_button);
        learningButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(learningIntent);
            }
        });

        Button practiceButton = (Button)findViewById(R.id.practice_button);
        practiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(practiceIntent);
            }
        });

        Button transmitButton = (Button)findViewById(R.id.transmit_button);
        transmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(transmitIntent);
            }
        });

        Button encodeButton = (Button)findViewById(R.id.encode_button);
        encodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(encodeIntent);
            }
        });

        Button settingsButton = (Button)findViewById(R.id.settings_button);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(settingsIntent);
            }
        });

//        Button aboutButton = (Button)findViewById(R.id.about_button);
//        aboutButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(aboutIntent);
//            }
//        });

        Button referenceButton = (Button)findViewById(R.id.reference_button);
        referenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(referenceIntent);
            }
        });

    }

}
