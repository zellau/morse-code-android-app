package com.zellaurquhart.android.morsecode;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

//backbone of this class from http://stackoverflow.com/questions/7089313/android-listen-for-incoming-sms-messages
public class SMSListener extends BroadcastReceiver {

    private SharedPreferences preferences;
    MorseCodeSound playMorse;

    @Override
    public void onReceive(Context context, Intent intent) {
        playMorse = new MorseCodeSound(context, AudioManager.STREAM_DTMF);
        if (((AudioManager)context.getSystemService(Context.AUDIO_SERVICE)).getRingerMode() == AudioManager.RINGER_MODE_VIBRATE){
            playMorse.setVibrateOn(true);
        }

        preferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        int tone = preferences.getInt("Tone", 0);

        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            Bundle bundle = intent.getExtras();           //---get the SMS message passed in---
            SmsMessage[] msgs = null;
            String msg_from;
            if (bundle != null){
                //---retrieve the SMS message received---
                try{
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for(int i=0; i<msgs.length; i++){
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        //String sender = msgs[i].
                        Uri message = Uri.parse("content://sms/");
                        ContentResolver cr = context.getContentResolver();
                        Cursor c = cr.query(message, null, null, null, null);
                        msg_from = msgs[i].getOriginatingAddress();
                        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(msg_from));
                        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
                        String contactName = null;
                        if (cursor != null) {
                            contactName = null;
                            if (cursor.moveToFirst()) {
                                contactName = cursor.getString(cursor
                                        .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
                            }
                        }
                        String msgBody = msgs[i].getMessageBody();
                        if (tone == 1){
                            if (contactName != null){
                                playMorse.startNewSound(contactName);
                            } else {
                                playMorse.startNewSound(msg_from);
                            }
                        } else if (tone == 2){
                            playMorse.startNewSound(msgBody);
                        }
                    }
                }catch(Exception e){
                            Log.d("Exception caught", e.getMessage());
                }
            }
        }
    }
}