package com.zellaurquhart.android.morsecode;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class PracticeActivity extends AppCompatActivity {
    String input = "";
    String message = "";
    TextView secretMessage;
    ImageButton nextButton;
    int level;
    boolean soundOn;
    boolean lightOn;
    boolean vibrateOn;
    boolean hintsOn;
    ArrayList<Integer> selected = new ArrayList<Integer>();
    char hide_char;
    int onColor = Color.LTGRAY;
    int offColor = Color.TRANSPARENT;

    //easy mode: practice characters
    Character[] characters = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
            'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', ',', '?', '!'};
    //medium mode: practice words
    int[] words = {R.string.word1, R.string.word2, R.string.word3, R.string.word4, R.string.word5, R.string.word6, R.string.word7, R.string.word8,
            R.string.word9, R.string.word10, R.string.word11, R.string.word12, R.string.word13, R.string.word14, R.string.word15, R.string.word16,
            R.string.word17, R.string.word18, R.string.word19, R.string.word20, R.string.word21, R.string.word22, R.string.word23, R.string.word24,
            R.string.word25, R.string.word26, R.string.word27, R.string.word28, R.string.word29, R.string.word30, R.string.word31,
            R.string.word32, R.string.word33, R.string.word34, R.string.word35, R.string.word36, R.string.word37, R.string.word38, R.string.word39,
            R.string.word40, R.string.word41, R.string.word42, R.string.word43, R.string.word44, R.string.word45, R.string.word46, R.string.word47,
            R.string.word48, R.string.word49, R.string.word50, R.string.word51, R.string.word52, R.string.word53, R.string.word54, R.string.word55,
            R.string.word56, R.string.word57, R.string.word58, R.string.word59, R.string.word60, R.string.word61, R.string.word62, R.string.word63,
            R.string.word64, R.string.word65, R.string.word66, R.string.word67, R.string.word68, R.string.word69, R.string.word70, R.string.word71,
            R.string.word72, R.string.word73, R.string.word74, R.string.word75, R.string.word76, R.string.word77, R.string.word78, R.string.word79,
            R.string.word80, R.string.word81, R.string.word82, R.string.word83, R.string.word84, R.string.word85, R.string.word86, R.string.word87,
            R.string.word88, R.string.word89, R.string.word90, R.string.word91, R.string.word92, R.string.word93, R.string.word94, R.string.word95,
            R.string.word96, R.string.word97, R.string.word98, R.string.word99, R.string.word100, R.string.word101, R.string.word102, R.string.word103,
            R.string.word104, R.string.word105, R.string.word106, R.string.word107, R.string.word108, R.string.word109, R.string.word110, R.string.word111,
            R.string.word112, R.string.word113, R.string.word114, R.string.word115, R.string.word116, R.string.word117, R.string.word118, R.string.word119,
            R.string.word120, R.string.word121, R.string.word122, R.string.word123, R.string.word124, R.string.word125, R.string.word126, R.string.word127,
            R.string.word128, R.string.word129, R.string.word130, R.string.word131, R.string.word132, R.string.word133, R.string.word134, R.string.word135,
            R.string.word136, R.string.word137, R.string.word138, R.string.word139, R.string.word140, R.string.word141, R.string.word142, R.string.word143,
            R.string.word144, R.string.word145, R.string.word146, R.string.word147, R.string.word148, R.string.word149, R.string.word150, R.string.word151,
            R.string.word152, R.string.word153, R.string.word154, R.string.word155, R.string.word156, R.string.word157, R.string.word158, R.string.word159,
            R.string.word160, R.string.word161, R.string.word162, R.string.word163, R.string.word164, R.string.word165, R.string.word166, R.string.word167,
            R.string.word168, R.string.word169, R.string.word170, R.string.word171, R.string.word172, R.string.word173, R.string.word174, R.string.word175,
            R.string.word176, R.string.word177, R.string.word178, R.string.word179, R.string.word180, R.string.word181, R.string.word182, R.string.word183,
            R.string.word184, R.string.word185, R.string.word186, R.string.word187, R.string.word188, R.string.word189, R.string.word190, R.string.word191,
            R.string.word192, R.string.word193, R.string.word194, R.string.word195, R.string.word196, R.string.word197, R.string.word198, R.string.word199,
            R.string.word200, R.string.word201, R.string.word202, R.string.word203, R.string.word204, R.string.word205, R.string.word206};
    //hard mode: practice entire phrases or sentences
    int[] phrases = {R.string.phrase1, R.string.phrase2, R.string.phrase3, R.string.phrase4, R.string.phrase5, R.string.phrase6, R.string.phrase7,
            R.string.phrase8, R.string.phrase9, R.string.phrase10, R.string.phrase11, R.string.phrase12, R.string.phrase13, R.string.phrase14,
            R.string.phrase15, R.string.phrase16, R.string.phrase17, R.string.phrase18, R.string.phrase19, R.string.phrase20, R.string.phrase21,
            R.string.phrase22, R.string.phrase23, R.string.phrase24, R.string.phrase25, R.string.phrase26, R.string.phrase27, R.string.phrase28,
            R.string.phrase29, R.string.phrase30, R.string.phrase31, R.string.phrase32, R.string.phrase33, R.string.phrase34, R.string.phrase35,
            R.string.phrase36, R.string.phrase37, R.string.phrase38, R.string.phrase39, R.string.phrase40, R.string.phrase41, R.string.phrase42,
            R.string.phrase43, R.string.phrase44, R.string.phrase45, R.string.phrase46, R.string.phrase47, R.string.phrase48, R.string.phrase49,
            R.string.phrase50, R.string.phrase51, R.string.phrase52, R.string.phrase53, R.string.phrase54, R.string.phrase55, R.string.phrase56,
            R.string.phrase57, R.string.phrase58, R.string.phrase59, R.string.phrase60, R.string.phrase61, R.string.phrase62, R.string.phrase63,
            R.string.phrase64, R.string.phrase65, R.string.phrase66, R.string.phrase67, R.string.phrase68, R.string.phrase69, R.string.phrase70,
            R.string.phrase71, R.string.phrase72, R.string.phrase73, R.string.phrase74, R.string.phrase75, R.string.phrase76, R.string.phrase77,
            R.string.phrase78, R.string.phrase79, R.string.phrase80, R.string.phrase81, R.string.phrase82, R.string.phrase83, R.string.phrase84,
            R.string.phrase85, R.string.phrase86, R.string.phrase87, R.string.phrase88, R.string.phrase89, R.string.phrase90, R.string.phrase91,
            R.string.phrase92, R.string.phrase93, R.string.phrase94, R.string.phrase95, R.string.phrase96, R.string.phrase97, R.string.phrase98,
            R.string.phrase99, R.string.phrase100, R.string.phrase101, R.string.phrase102, R.string.phrase103, R.string.phrase104, R.string.phrase105,
            R.string.phrase106};

    MorseCodeSound sounds;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sounds = new MorseCodeSound(this, AudioManager.STREAM_MUSIC);
        setContentView(R.layout.activity_practice);

        final SharedPreferences preferences = getSharedPreferences("settings", MODE_PRIVATE);
        level = preferences.getInt("Difficulty", 0);
        soundOn = preferences.getBoolean("SoundOn", true);
        lightOn = preferences.getBoolean("LightOn", false);
        vibrateOn = preferences.getBoolean("VibrateOn", false);
        hintsOn = preferences.getBoolean("HintsOn", false);
        sounds.setSoundOn(soundOn);
        sounds.setLightOn(lightOn);
        sounds.setVibrateOn(vibrateOn);

        MenuBar menuBar = new MenuBar(this, "Practice");
        menuBar.setId(10);
        RelativeLayout layout = (RelativeLayout)findViewById(R.id.layout);
        layout.addView(menuBar);
        menuBar.setBackListener(new MenuBar.BackListener() {
            @Override
            public void backListener() {
                finish();
            }
        });

        ImageButton replayButton = (ImageButton)findViewById(R.id.replay_button);
        replayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sounds.startNewSound(message);
            }
        });

        ImageButton hintButton = (ImageButton)findViewById(R.id.hint_button);
        hintButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hintsOn){
                    hintsOn = false;
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("HintsOn", false);
                    editor.commit();
                } else {
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("HintsOn", true);
                    editor.commit();
                    hintsOn = true;
                }
                String seenOnScreen = input + hideSecret(message.substring(Math.min(input.length(), message.length())));
                secretMessage.setText(seenOnScreen);
            }
        });

        ImageButton audioButton = (ImageButton)findViewById(R.id.audio_button);
        if (soundOn){
            audioButton.setBackgroundColor(onColor);
        }
        audioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sounds.getSound()) {
                    sounds.setSoundOn(false);
                    v.setBackgroundColor(offColor);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("SoundOn", false);
                    editor.commit();
                } else {
                    sounds.setSoundOn(true);
                    v.setBackgroundColor(onColor);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("SoundOn", true);
                    editor.commit();
                }
            }
        });

        ImageButton lightButton = (ImageButton)findViewById(R.id.light_button);
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            if (lightOn) {
                lightButton.setBackgroundColor(onColor);
            }
            lightButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (sounds.getLight()) {
                        sounds.setLightOn(false);
                        v.setBackgroundColor(offColor);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("LightOn", false);
                        editor.commit();
                    } else {
                        sounds.setLightOn(true);
                        v.setBackgroundColor(onColor);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean("LightOn", true);
                        editor.commit();
                    }
                }
            });
        } else {
            layout.removeView(lightButton);
            RelativeLayout.LayoutParams audioLayout = (RelativeLayout.LayoutParams)audioButton.getLayoutParams();
            audioLayout.addRule(RelativeLayout.RIGHT_OF, replayButton.getId());
        }


        ImageButton vibrateButton = (ImageButton)findViewById(R.id.vibrate_button);
        if (vibrateOn){
            vibrateButton.setBackgroundColor(onColor);
        }
        vibrateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sounds.getVibrate()) {
                    sounds.setVibrateOn(false);
                    v.setBackgroundColor(offColor);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("VibrateOn", false);
                    editor.commit();
                } else {
                    sounds.setVibrateOn(true);
                    v.setBackgroundColor(onColor);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("VibrateOn", true);
                    editor.commit();
                }
            }
        });

        nextButton = (ImageButton)findViewById(R.id.next_button);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setImageResource(R.drawable.empty);
                nextButton.setClickable(false);
                secretMessage.setTextColor(Color.BLACK);
                input = "";
                pickSecret();
                secretMessage.setText(hideSecret(message));
                sounds.startNewSound(message);
            }
        });

        pickSecret();
        secretMessage = new TextView(this);
        RelativeLayout.LayoutParams messageParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        messageParams.addRule(RelativeLayout.BELOW, 10);
        if (Build.VERSION.SDK_INT > 20) {
            hide_char = '_';
            secretMessage.setLetterSpacing(.08f);
        } else {
            hide_char = '?';
        }
        secretMessage.setText(hideSecret(message));
        secretMessage.setTextSize(25);
        secretMessage.setTextColor(Color.BLACK);
        layout.addView(secretMessage, messageParams);

        sounds.startNewSound(message);

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event){
        boolean ret = super.dispatchKeyEvent(event);
        if(event.getAction() == KeyEvent.ACTION_UP && event.getKeyCode() != KeyEvent.KEYCODE_VOLUME_DOWN &&
                event.getKeyCode() != KeyEvent.KEYCODE_VOLUME_UP && event.getKeyCode() != KeyEvent.KEYCODE_VOLUME_MUTE){
            if (event.getKeyCode() == KeyEvent.KEYCODE_DEL){
                if (input.length() > 0){
                    input = input.substring(0, input.length()-1);
                }
            } else {
                input = input + (char)event.getUnicodeChar();
            }
        }
        onTextChange();
        return ret;
    }

    private void onTextChange() {
        String seenOnScreen = input + hideSecret(message.substring(Math.min(input.length(), message.length())));
        secretMessage.setText(seenOnScreen);
        if (input.toLowerCase().equals(message.toLowerCase())) {
            secretMessage.setTextColor(Color.GREEN);
            nextButton.setImageResource(R.drawable.forward_arrow2);
            nextButton.setClickable(true);
        } else if (input.length() < message.length() && input.toLowerCase().equals(message.substring(0, input.length()).toLowerCase())){
            secretMessage.setTextColor(Color.BLACK);
        } else {
            secretMessage.setTextColor(Color.RED);
            //spaces automatically get added after ? and !. It's very annoying
            if (input.length()>1 && (input.substring(input.length()-2, input.length()-1).equals("!") || input.substring(input.length()-2, input.length()-1).equals("?"))){
                input = input.substring(0, input.length()-1);
                onTextChange();
            }
        }
    }

    public void pickSecret(){
        int random;
        if (level == 0){
            if (selected.size() == characters.length){
                selected.clear();
            }
            random = (int)Math.floor(Math.random()*characters.length);
            if (selected.contains(random)){
                pickSecret();
            } else {
                message = ""+characters[random];
            }
        } else if (level == 1){
            if (selected.size() == words.length){
                selected.clear();
            }
            random = (int)Math.floor(Math.random()*words.length);
            if (selected.contains(random)){
                pickSecret();
            } else {
                message = getString(words[random]);
            }
        } else {
            if (selected.size() == phrases.length){
                selected.clear();
            }
            random = (int)Math.floor(Math.random()*phrases.length);
            if (selected.contains(random)){
                pickSecret();
            } else {
                message = getString(phrases[random]);
            }
        }
        selected.add(random);
    }

    public String hideSecret(String secretMessage){
        String hidden = "";
        if (hintsOn) {
            for (int i = 0; i < secretMessage.length(); i++) {
                if (secretMessage.charAt(i) == ' ') {
                    hidden += ' ';
                } else {
                    hidden += hide_char;
                }
            }
        }
        return hidden;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        sounds.destroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        outState.putString("message", message);
        outState.putString("input", input);
        outState.putIntegerArrayList("selected", selected);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle inState){
        super.onRestoreInstanceState(inState);
        message = inState.getString("message");
        input = inState.getString("input");
        onTextChange();
        selected = inState.getIntegerArrayList("selected");
        sounds.startNewSound(message);
    }

}
