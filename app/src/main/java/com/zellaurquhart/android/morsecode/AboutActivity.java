package com.zellaurquhart.android.morsecode;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Zella on 12/14/15.
 */
public class AboutActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        MenuBar menuBar = (MenuBar)findViewById(R.id.menu_bar);
        menuBar.setName("About");
        menuBar.setBackListener(new MenuBar.BackListener() {
            @Override
            public void backListener() {
                finish();
            }
        });


    }

}
